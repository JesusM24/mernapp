import axios from "axios";

export const getTasksRequest = async () =>
  axios.get("http://localhost:4000/tasks");

export const createTaskRequest = async (task) =>
  axios.post("http://localhost:4000/tasks", task);

export const deleteTaskRequest = async (id) =>
  axios.delete(`http://localhost:4000/tasks/${id}`);

export const getTaskRequest = async (id) =>
  axios.get(`http://localhost:4000/tasks/${id}`);

export const updateTaskRequest = async (id, newFields) =>
  axios.put(`http://localhost:4000/tasks/${id}`, newFields);
