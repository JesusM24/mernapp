import { Form, Formik } from "formik";
import { UseTasks } from "../context/TaskProvider";
import { useNavigate, useParams } from "react-router-dom";
import { useEffect, useState } from "react";

function TasksForm() {
  const { createTask, getTask, updateTask } = UseTasks();

  const [task,setTask] = useState({
    title: "",
    descripcion: "",
  });

  const params = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    const loadTask = async () => {
      if (params.id) {
        const task = await getTask(params.id);
        setTask({
          title: task.title,
          descripcion: task.descripcion,
        })
      }
    };
    loadTask();
  }, []);

  return (
    <div>
      <h1>{params.id ? "Edit Task" : "New Task"}</h1>

      <Formik
        initialValues={task}
        enableReinitialize={true}
        onSubmit={async (values, actions) => {
          console.log(values);

          if(params.id) {
            await updateTask(params.id, values)
            navigate("/");
          } else {
            await createTask(values);
          }

          setTask({
            title:"",
            descripcion:"",
          });
        }}
      >
        {({ handleChange, handleSubmit, values, isSubmitting }) => (
          <Form onSubmit={handleSubmit}>
            <label>title</label>
            <input
              type="text"
              name="title"
              placeholder="Write a description"
              onChange={handleChange}
              value={values.title}
            />
            <label>descripcion</label>
            <textarea
              name="descripcion"
              rows="3"
              placeholder="Write a description"
              onChange={handleChange}
              value={values.descripcion}
            ></textarea>

            <button type="submit" disabled={isSubmitting}>
              {isSubmitting ? "Saving..." : "Save"}
            </button>
          </Form>
        )}
      </Formik>
    </div>
  );
}

export default TasksForm;
