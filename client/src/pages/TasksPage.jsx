import { useEffect } from "react";
import TaskCard from "../components/TaskCard";
import { UseTasks } from "../context/TaskProvider";

function TasksPage() {
  
  const {tasks, loadTasks} = UseTasks();

  useEffect(() => {
    loadTasks();
  }, []);

  function renderMain() {
    if (tasks.length === 0) return <h1>not tasks yet</h1>;
    return tasks.map((task) => <TaskCard task={task} key={task.id} />);
  }
  return (
    <div>
      <h1>Tasks</h1>
      {renderMain()}
    </div>
  );
}

export default TasksPage;
